<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    use HasFactory;

    protected $table = 'accounts';
    protected $primaryKey = 'account_id';
    protected $fillable = ['account_name','parent_account_id','account_level'];

    public static function getAccountName($id){
        $account =  Accounts::find($id);
        return $account->account_name??'';
    }
}
