<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    
    protected $primaryKey = 'id';
    protected $fillable = ['name','parentProductId','salesPrice','purchasePrice',
                            'categoryId','unitId','productType'];



    public function UnitName(){
        return $this->belongsTo('App\Models\SystemFlags', 'unitId');
    }

    public function CategoryName(){
        return $this->belongsTo('App\Models\SystemFlags', 'categoryId');
    }

    public function ParentProductName(){
        return $this->belongsTo('App\Models\products', 'parentProductId');
    }
}
