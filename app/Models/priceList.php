<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class priceList extends Model
{
    use HasFactory;

    protected $table = 'price_lists';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id','salesPrice', 'created_at', 'updated_at'];

    public function ProductName(){
        return $this->belongsTo('App\Models\products', 'product_id');
    }
}
