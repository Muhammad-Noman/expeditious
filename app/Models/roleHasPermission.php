<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class roleHasPermission extends Model
{
    use HasFactory;

    protected $table = 'role_has_permissions';

    public function RoleName(){
        return $this->belongsTo('Spatie\Permission\Models\Role', 'role_id');
    }

    public function PermissionName(){
        return $this->belongsTo('Spatie\Permission\Models\Permission', 'permission_id');
    }
}
