<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemFlags extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $fillable = ['flagName','flagType','modifiedBy','createdBy','parentId'];

   public static function getSystemFlagName($id){

        $systemFlag =  SystemFlags::find($id);
        return $systemFlag->flagName??'';
   }
}
