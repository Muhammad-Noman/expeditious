<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class userHasRoles extends Model
{
    use HasFactory;

    protected $table = 'model_has_roles';

    public function UserName(){
        return $this->belongsTo('App\Models\User', 'model_id');
    }

    public function RoleName(){
        return $this->belongsTo('Spatie\Permission\Models\Role', 'role_id');
    }
}
