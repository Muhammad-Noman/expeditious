<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoice';
    protected $primaryKey = 'invoice_id';
    protected $fillable = ['invoice_document_id','invoice_parent_docuemt_id','invoice_total_amount',
                        'invoice_disccount_amount','invoice_disccount_percentage','invoice_stax_amount',
                        'invoice_stax_percentage','invoice_additiontax_amount','invoice_additiontax_percentage',
                        'invoice_customer_id','remarks','invoice_date','invoice_exclusive_amount','invoice_type'];


    public static function getNewDocumentNo($doctype){

        $documentNo = DB::table('invoice')->select(DB::raw('ifnull(max(invoice_document_id)+1,1) as DocumentNo'))->where('invoice_type',$doctype)->pluck('DocumentNo');
        // var_dump($documentNo);die();
        return $documentNo[0];

    }

}
