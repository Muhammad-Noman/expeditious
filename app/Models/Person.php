<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class person extends Model
{
    use HasFactory;

    protected $table = 'people';
    protected $primaryKey = 'person_id';
    protected $fillable = ['name','parent_customer_id','dob','contact_no','contact_no_2','email',
                            'joining_date','personType','address'];


    public static function getPersonName($id){

        $person =  Person::find($id);
        return $person->name??'';
    }

}
