<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory;

    protected $table = 'invoice_items';
    protected $primaryKey = 'invoice_item_id';
    protected $fillable = ['invoice_id','product_id','invoice_item_price',
                        'invoice_item_qty','invoice_item_loose_qty','invoice_item_line_amount',
                        'invoice_item_disccount_amount','invoice_item_disccount_percentage','invoice_item_stax_amount',
                        'invoice_item_stax_percentage','remarks','invoice_date'];

    public function ProductName(){
        return $this->belongsTo('App\Models\products', 'product_id');
    }
}
