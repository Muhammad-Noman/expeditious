<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\Person;
use Config;
use LaravelDaily\Invoices\Invoice as PrintInvoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem as PrintInvoiceItem;
use Carbon\Carbon;

class InvoiceController extends Controller
{
    //
    public function printSalesInvoice($id){
        $invoiceInfo = Invoice::find($id);
        $customerInfo = Person::find($invoiceInfo->invoice_customer_id);
        $customer = new Buyer([
            'name'          => $customerInfo->name,
            'custom_fields' => [
                'email'=>$customerInfo->email,
                'contact_no' => $customerInfo->contact_no,
            ],
        ]);
        $invoiceItems = InvoiceItem::where(['invoice_id'=>$invoiceInfo->invoice_id])->get();
        $invoice = PrintInvoice::make()
            ->buyer($customer)
            ->discountByPercent($invoiceInfo->invoice_disccount_percentage)
            ->taxRate($invoiceInfo->invoice_stax_percentage)
            ->currencySymbol('Rs')
            ->currencyCode('PKR')
            ->logo(public_path('vendor/invoices/sample-logo.png'))
            ->date(Carbon::parse($invoiceInfo->invoice_date))
            ->sequence($invoiceInfo->invoice_document_id)
            ->serialNumberFormat('{SEQUENCE}');
                     

        foreach($invoiceItems as $invoiceItemInfo){
            $item = (new PrintInvoiceItem())
                    ->title($invoiceItemInfo->ProductName->name)                    
                    ->quantity($invoiceItemInfo->invoice_item_qty)
                    ->pricePerUnit($invoiceItemInfo->invoice_item_price)
                    ->discountByPercent($invoiceItemInfo->invoice_item_disccount_percentage);
            $invoice->addItem($item);
        }
        

        
            return $invoice->stream();
    }
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'invoice_customer_id' => 'required|integer',
            'invoice_type' => 'required|string',
            'invoice_exclusive_amount' => 'required|numeric|min:1',            
            'invoice_disccount_percentage' => 'required|numeric|min:0',            
            'invoice_stax_percentage' => 'required|numeric|min:0',            
            'invoice_disccount_amount' => 'required|numeric|min:0',            
            'invoice_stax_amount' => 'required|numeric|min:0',            
            'invoice_total_amount' => 'required|numeric|min:1',
            'invoice_date'=>'required',            
            'remarks' => 'nullable',
            'invoice_parent_docuemt_id'=>'nullable|numeric',
        ]);

        if(!empty($request->invoice_id)){
            $invoice = Invoice::find($request->invoice_id);
            $invoice->fill($validateData);
            $invoice->updated_at = date("Y-m-d h:i:s");
            $invoice->modifiedBy = auth()->user()->name;
            if($invoice->save()){
                foreach ($request['invoiceItem'] as $value) {
                    if (!empty($value['invoice_item_id'])){
                        $invoiceItem = InvoiceItem::find($value['invoice_item_id']);
                        $invoiceItem->fill($value);
                        if($invoiceItem->invoice_item_qty > 0 && $invoiceItem->product_id > 0){
                            $invoiceItem->save();
                        }else{
                            InvoiceItem::where('invoice_item_id', $value['invoice_item_id'])->delete();
                        }
                    }else{
                        $invoiceItem = new InvoiceItem();
                        $invoiceItem->fill($value);
                        $invoiceItem->invoice_id = $invoice->invoice_id;
                        if($invoiceItem->invoice_item_qty > 0 && $invoiceItem->product_id > 0){
                            $invoiceItem->save();
                        }

                    }
                }                
                $model = new Invoice();
                $model->invoice_type = $invoice->invoice_type;
                if($model->invoice_type == Config::get('constants.constants.salesInvoice') ){
                    return redirect()->route('invoice.salesInvoice', ['model'=>$model])->with('success', 'Invoice created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.salesOrder') ){
                    return redirect()->route('invoice.salesOrder', ['model'=>$model])->with('success', 'Order created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseOrder') ){
                    return redirect()->route('invoice.purchaseOrder', ['model'=>$model])->with('success', 'Order created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseInvoice') ){
                    return redirect()->route('invoice.purchaseInvoice', ['model'=>$model])->with('success', 'Invoice created successfully');
                }
            }else{
                $model = new Invoice();
                $model->invoice_type = $invoice->invoice_type;
                if($model->invoice_type == Config::get('constants.constants.salesInvoice') ){
                    return redirect()->route('invoice.salesInvoice', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.salesOrder') ){
                    return redirect()->route('invoice.salesOrder', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseOrder') ){
                    return redirect()->route('invoice.purchaseOrder', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseInvoice') ){
                    return redirect()->route('invoice.purchaseInvoice', ['model'=>$model])->with('error', 'Error Occured');
                }
            }
        }else{
            $invoice = new Invoice();
            $invoice->fill($validateData);
            $invoice->created_at = date("Y-m-d h:i:s");
            $invoice->createdBy = auth()->user()->name;
            $invoice->invoice_document_id = Invoice::getNewDocumentNo($invoice->invoice_type);
            if($invoice->save()){
                foreach ($request['invoiceItem'] as $value) {
                    $invoiceItem = new InvoiceItem();
                    $invoiceItem->fill($value);
                    $invoiceItem->invoice_id = $invoice->invoice_id;
                    if($invoiceItem->invoice_item_qty > 0 && $invoiceItem->product_id > 0){
                        $invoiceItem->save();
                    }
                }
                $model = new Invoice();
                $model->invoice_type = $invoice->invoice_type;
                if($model->invoice_type == Config::get('constants.constants.salesInvoice') ){
                    return redirect()->route('invoice.salesInvoice', ['model'=>$model])->with('success', 'Invoice created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.salesOrder') ){
                    return redirect()->route('invoice.salesOrder', ['model'=>$model])->with('success', 'Order created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseOrder') ){
                    return redirect()->route('invoice.purchaseOrder', ['model'=>$model])->with('success', 'Order created successfully');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseInvoice') ){
                    return redirect()->route('invoice.purchaseInvoice', ['model'=>$model])->with('success', 'Invoice created successfully');
                }

            }else{
                $model = new Invoice();
                $model->invoice_type = $invoice->invoice_type;
                if($model->invoice_type == Config::get('constants.constants.salesInvoice') ){
                    return redirect()->route('invoice.salesInvoice', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.salesOrder') ){
                    return redirect()->route('invoice.salesOrder', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseOrder') ){
                    return redirect()->route('invoice.purchaseOrder', ['model'=>$model])->with('error', 'Error Occured');
                }
                if($model->invoice_type == Config::get('constants.constants.purchaseInvoice') ){
                    return redirect()->route('invoice.purchaseInvoice', ['model'=>$model])->with('error', 'Error Occured');
                }
            }
        }
        
            
        
    }
    //Sales Invoice Against Sales Order
    public function SIASO(){
        
        $model = new Invoice();
        return view('invoice.SIASO', ['model'=>$model]);
    }

    public function PIAPO(){
        
        $model = new Invoice();
        return view('invoice.PIAPO', ['model'=>$model]);
    }

    public function show($id){
        $invoice = Invoice::find($id);
        
        Invoice::where('invoice_id', $id)->delete();
        InvoiceItem::where('invoice_id', $id)->delete();
        if($invoice->invoice_type == Config::get('constants.constants.salesOrder') ){
            
            $model = new Invoice();
            $model->invoice_type = Config::get('constants.constants.salesOrder');
            return redirect()->route('invoice.salesOrder', ['model'=>$model])->with('success', 'Deleted Successfully');
        }
        else if($invoice->invoice_type == Config::get('constants.constants.salesInvoice') ){
            $model = new Invoice();
            $model->invoice_type = Config::get('constants.constants.salesInvoice');
            return redirect()->route('invoice.salesInvoice', ['model'=>$model])->with('success', 'Deleted Successfully');
        }
        else if($invoice->invoice_type == Config::get('constants.constants.purchaseOrder') ){
            $model = new Invoice();
            $model->invoice_type = Config::get('constants.constants.purchaseOrder');
            return redirect()->route('invoice.purchaseOrder', ['model'=>$model])->with('success', 'Deleted Successfully');
        }
        else if($invoice->invoice_type == Config::get('constants.constants.purchaseInvoice') ){
            $model = new Invoice();
            $model->invoice_type = Config::get('constants.constants.purchaseInvoice');
            return redirect()->route('invoice.purchaseInvoice', ['model'=>$model])->with('success', 'Deleted Successfully');
        }
        else{
            return Redirect::back()->with(['error', 'Kindly contact with support person.']);
        }
    }

    public function edit($id){

    }

    public function destory($id){
        // it goes to show method
    }

    public function salesInvoice(){
        $model = new Invoice();
        $model->invoice_type = Config::get('constants.constants.salesInvoice');
        return view('invoice.salesInvoice', ['model'=>$model]);
    }

    public function salesOrder(){
        $model = new Invoice();
        $model->invoice_type = Config::get('constants.constants.salesOrder');
        return view('invoice.salesOrder', ['model'=>$model]);
    }

    public function PurchaseInvoice(){
        $model = new Invoice();
        $model->invoice_type = Config::get('constants.constants.purchaseInvoice');
        return view('invoice.purchaseInvoice', ['model'=>$model]);
    }
    public function PurchaseOrder(){
        $model = new Invoice();
        $model->invoice_type = Config::get('constants.constants.purchaseOrder');
        return view('invoice.purchaseOrder', ['model'=>$model]);
    }
}
