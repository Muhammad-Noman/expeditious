<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class Select2SearchController extends Controller
{
    public function index()
    {
    	return view('home');
    }

    public function selectSearch(Request $request)
    {
    	//var_dump($request);
        if($request->has('table')){
            $search = $request->search;
            $query = "select ".$_REQUEST['column1'].",".$_REQUEST['column2']." from ".$_REQUEST['table']
            ." where ".$_REQUEST['column2']." LIKE '%$search%'";

            if($request->has('where')){
                $query .= "and ".$_REQUEST['where'];
            }
            $data = DB::select($query);
        }
        return response()->json($data);
    }
}
