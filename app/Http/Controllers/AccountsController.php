<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Accounts;

class AccountsController extends Controller
{
    public function __construct(Accounts $account)
    {
        $this->middleware("auth");
    }

    public function show($id)
    {
        $Data = Accounts::find($id);
        
        Accounts::where('accounts_id', $id)->delete();
        $model = new Accounts();
        $model->account_level = 4;

        return redirect()->route('accounts.accountHead', ['model'=>$model])->with('success',$Data->accounts_name.'  deleted');
        
    }  

    public function destroy($id)
    {
        //it goes to show method
    }

    public function edit($id)
    {
        return redirect()->route('accounts.accountHead', ['id'=>$id]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'account_name' => 'required|string',
            'parent_account_id' => 'nullable|numeric',
            'account_level' => 'required|numeric'
        ]);
        
        if(!empty($request->account_id)){
            $account = Accounts::find($request->account_id);
            $account->fill($validateData);
            $account->updated_at = date("Y-m-d h:i:s");
            $account->modifiedBy = auth()->user()->name;
            $model = new Accounts();
            $model->account_level = $account->account_level;
            if($account->save()){
                return redirect()->route('accounts.accountHead', ['model'=>$model])->with('success', $request["account_name"].' Updated');                
            }else{
                return Redirect::back()->with('error', 'Error Occured');
            }

        }else{
            $account = new Accounts();

            $account->fill($validateData);
            $account->created_at = date("Y-m-d h:i:s");
            $account->createdBy = auth()->user()->name;
            $model = new Accounts();
            $model->account_level = $account->account_level;
            if($account->save()){
                return redirect()->route('accounts.accountHead', ['model'=>$model])->with('success', $request["account_name"].' Created');                
            }else{
                return Redirect::back()->with('error', 'Error Occured');
            }
        }
        
        
    }

    public function accountHead()
    {
        $model = new Accounts();
        $model->account_level = 4;
        return view('accounts.accountHead', ['model'=>$model]);
    }
}
