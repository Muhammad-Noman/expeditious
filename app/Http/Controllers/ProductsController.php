<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use Config;

class ProductsController extends Controller
{

    public function __construct(products $products)
    {
        $this->middleware("auth");
    }

    public function show($id)
    {
        $Data = Products::find($id);
        
        $productType = $Data->productType;

        Products::where('id', $id)->delete();
        $model = new Products();
        $model->productType = $productType;
        return redirect()->route('products.products',['model'=>$model])->with('success',$Data->name.'  deleted');
        
    }    
    

    public function destroy($id)
    {
        //it goes to show method
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string',
            'parentProductId' => 'nullable',
            'salesPrice' => 'nullable',
            'purchasePrice' => 'nullable',
            'productType' => 'string',
            'unitId' => 'required|integer',
            'categoryId' => 'required|integer'
            
        ]);

        
        if(!empty($request->id)){
            $products = Products::find($request->id);
            $products->fill($validateData);
            $products->updated_at = date("Y-m-d h:i:s");
            $products->modifiedBy = auth()->user()->name;
            if($products->save()){
                $model = new Products();
                $model->productType = $products->productType;        
                return redirect()->route('products.products', ['model'=>$model])->with('success', $request["name"].' Updated');
            }else{
                return redirect()->route('products.products', ['model'=>$products])->with('error', 'Error Occured');
            }

        }else{
            $products = new Products();
            
            $products->fill($validateData);
            $products->created_at = date("Y-m-d h:i:s");
            $products->createdBy = auth()->user()->name;
            
            if($products->save()){
                $model = new Products();
                $model->productType = $products->productType;        
                return redirect()->route('products.products', ['model'=>$model])->with('success', $request["name"].' Created');
            }else{
                return redirect()->route('products.products', ['model'=>$products])->with('error', 'Error Occured');
            }
        }
        
        
    }

    public function edit($id)
    {
        $model = Products::find($id);
        return view('products.products', ['model'=>$model]);
        
    }

    public function products()
    {
        $model = new Products();
        $model->productType = Config::get('constants.constants.finishProduct');
        return view('products.products', ['model'=>$model]);
    }

}
