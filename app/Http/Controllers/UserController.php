<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    ///Custom Function 
    public function profile(){
        return view("user.profile");
    }

    public function postProfile(Request $request){
        $user = auth()->user();
        $this->validate($request, [
            'name' => 'required',            
            'password' =>   'required|min:6|max:30' 
        ]);

        $value = $user->update([
            'name' => $request->name,
            'password' => bcrypt($request->password)
        ]);
        if($value > 0){
            return redirect()->back()->with('success', 'Profile Successfully Updated');
        }else{
            return redirect()->back()->with('error', 'Please contact Expeditious Support Team');
        }
        
    }

    public function addProfile(Request $request){
        // $user = auth()->user();
        $validateData = $request->validate([
            'name' => 'required|unique:users,name',  
            'email' => 'required|unique:users,email',            
            'password' =>   'required|min:6|max:30' 
        ]);

        $user = new User();
        $user->fill($validateData);        
        $user->password = bcrypt($request->password);
        if($user->save()){
            return redirect()->back()->with('success', 'Profile Successfully Updated');
        }else{
            return redirect()->back()->with('error', 'Please contact Expeditious Support Team');
        }
        
    }

    public function userRegistration(){
        return view("user.registration");
    }

}
