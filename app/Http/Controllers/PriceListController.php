<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\priceList;
use App\Models\Products;
use App\Models\priceListHistory;

use Config;

class PriceListController extends Controller
{
    public function PriceList(){
        $model = new priceList();
        return view('priceList.priceList', ['model'=>$model]);
    }

    public function ProductRateUpdate($id,$price){
       $name = auth()->user()->name;
        if(priceList::where('product_id', $id)->exists())
        {
               $pricelist = priceList::where('product_id', $id)->first();
               $pricelist->salesPrice = $price;
               $pricelist->save();

               $pricehistory = new priceListHistory();
               $pricehistory->product_id = $id;
               $pricehistory->salesPrice = $price;
               $pricehistory->createdBy = $name;
               $pricehistory->save();
               
               return "Price List Updated";
        } else {
               $pricelist = new priceList();   
               $pricelist->product_id = $id;
               $pricelist->salesPrice = $price;
               $pricelist->save();

               $pricehistory = new priceListHistory();
               $pricehistory->product_id = $id;
               $pricehistory->salesPrice = $price;
               $pricehistory->createdBy = $name;
               $pricehistory->save();
               
               return "Price List Updated";
        }
    }
}
