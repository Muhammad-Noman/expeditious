<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SystemFlags;

class SystemFlagsController extends Controller
{
    //

    public function __construct(SystemFlags $systemFlags)
    {
        $this->middleware("auth");
        $this->systemFlags = $systemFlags;
    }
    public function show($id)
    {
        $Data = systemFlags::find($id);

        if($Data->flagType == 'UNIT' ){
            SystemFlags::where('id', $id)->delete();
            $systemFlags = systemFlags::where('flagType','UNIT')->get();
            return redirect()->route('systemFlags.unit')->with('success',$Data->flagName.' Unit deleted');
        }
        else if($Data->flagType == 'CATEGORY' ){
            SystemFlags::where('id', $id)->delete();
            $systemFlags = systemFlags::where('flagType','CATEGORY')->get();
            return redirect()->route('systemFlags.category', ['systemFlag'=>'CATEGORY'])->with('success',$Data->flagName.' Category deleted');
        }
        else{
            return Redirect::back()->with(['error', 'Kindly contact with support person.']);
        }
    }    
    

    public function storeUnit(Request $request)
    {
        $request->validate([
            'flagName' => 'required|string',
            'flagType' => 'required|string'
        ]);

        if(!empty($request->id)){
            $systemFlags = systemFlags::find($request->id);
            $systemFlags->flagName = $request["flagName"];
            $systemFlags->updated_at = date("Y-m-d h:i:s");
            $systemFlags->modifiedBy = auth()->user()->name;
            if($systemFlags->save()){
                return redirect()->route('systemFlags.unit')->with('success', $request["flagName"].' Unit Updated');
            }else{
                return redirect()->route('systemFlags.unit')->with('error', 'Error Occured');
            }

        }else{
            $systemFlags = $this->systemFlags->create([
                'flagName' => $request->flagName,
                'createdBy' => auth()->user()->name,
                'modifiedBy' => auth()->user()->name,
                'flagType' => $request->flagType
            ]);
    
            return redirect()->route('systemFlags.unit')->with('success', $request["flagName"].' Unit Created');
        }
        
        
    }
    
    public function storeCategory(Request $request)
    {
        $request->validate([
            'flagName' => 'required|string',
            'flagType' => 'required|string'
        ]);

        
        if(!empty($request->id)){
            $systemFlags = systemFlags::find($request->id);
            $systemFlags->flagName = $request["flagName"];
            $systemFlags->updated_at = date("Y-m-d h:i:s");
            $systemFlags->parentId = $request["parentId"];
            $systemFlags->modifiedBy = auth()->user()->name;
            if($systemFlags->save()){
                return redirect()->route('systemFlags.category', ['systemFlag'=>'CATEGORY'])->with('success', $request["flagName"].' Category Updated');
            }else{
                return redirect()->route('systemFlags.category', ['systemFlag'=>'CATEGORY'])->with('error', 'Error Occured');
            }

        }else{
            $systemFlags = $this->systemFlags->create([
                'flagName' => $request->flagName,
                'parentId'=>$request->parentId,
                'createdBy' => auth()->user()->name,
                'modifiedBy' => auth()->user()->name,
                'flagType' => $request->flagType
            ]);
    
            return redirect()->route('systemFlags.category', ['systemFlag'=>'CATEGORY'])->with('success', $request["flagName"].' Category Created');
        }
        
        
    }

    // public function getAll(){
    //     $systemFlags = $this->systemFlags->all();
    //     return response()->json([
    //         'systemFlags' => $systemFlags
    //     ], 200);
    // }

    public function edit($id)
    {
        $Data = systemFlags::find($id);

        if($Data->flagType == 'UNIT' ){
            $systemFlags = systemFlags::where('flagType','UNIT')->get();
            return view('systemFlags.unit', ['units' => $systemFlags,'UnitData'=> $Data]);
        }
        else if($Data->flagType == 'CATEGORY' ){
            $systemFlags = systemFlags::where('flagType','CATEGORY')->get();
            //return view('systemFlags.category', ['categorys' => $systemFlags,'categoryData'=> $Data]);
            return redirect()->route('systemFlags.category', ['systemFlag'=>'CATEGORY','isEdit'=>'1','id'=>$id]);
        }
        else{
            return Redirect::back()->with(['error', 'Kindly contact with support person.']);
        }
        
            //->with('roleData', $roleData);
    }

    public function unit()
    {
        $systemFlags =systemFlags::where('flagType','UNIT')->get();
        return view('systemFlags.unit', ['units' => $systemFlags]);
    }

    public function category()
    {
        
        $systemFlags =systemFlags::where('flagType','CATEGORY')->get();
        if(isset($_GET['isEdit'])){
            $Data = systemFlags::find($_REQUEST['id']);
            return view('systemFlags.category', ['categorys' => $systemFlags,'categoryData'=> $Data]);
        }else{
            return view('systemFlags.category', ['categorys' => $systemFlags]);
        }
        
    }

    public function destroy($id)
    {
        //it goes to show method
    }

}
