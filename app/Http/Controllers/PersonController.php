<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use Config;


class PersonController extends Controller
{
    //
    public function __construct(Person $person)
    {
        $this->middleware("auth");
    }

    public function show($id)
    {
        $Data = Person::find($id);
        
        $personType = $Data->personType;
        Person::where('person_id', $id)->delete();
        $model = new Person();
        $model->personType = $personType;
      
        if($personType == Config::get('constants.constants.customer')){
            
            return redirect()->route('person.customers', ['model'=>$model])->with('success',$Data->name.'  deleted');
        }
    }  

    public function destroy($id)
    {
        //it goes to show method
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|string',
            'parent_customer_id' => 'nullable|numeric',
            'contact_no' => 'required|string',
            'contact_no_2' => 'nullable|string',
            'address' => 'nullable|string',
            'email' => 'required|string',
            'personType'=>'required|string'
            
        ]);
        
        if(!empty($request->person_id)){
            $person = Person::find($request->person_id);
            $person->fill($validateData);
            $person->updated_at = date("Y-m-d h:i:s");
            $person->modifiedBy = auth()->user()->name;
            $model = new Person();
            $model->personType = $person->personType;
            if($person->save()){
                if($person->personType == Config::get('constants.constants.customer')){
                    return redirect()->route('person.customers', ['model'=>$model])->with('success', $request["name"].' Updated');
                }else if($person->personType == Config::get('constants.constants.supplier')){
                    return redirect()->route('person.supplier', ['model'=>$model])->with('success', $request["name"].' Updated');
                }
            }else{
                return Redirect::back()->with('error', 'Error Occured');
            }

        }else{
            $person = new Person();
            
            $person->fill($validateData);
            $person->created_at = date("Y-m-d h:i:s");
            $person->createdBy = auth()->user()->name;
            $model = new Person();
            $model->personType = $person->personType;
            if($person->save()){
                if($person->personType == Config::get('constants.constants.customer')){
                    return redirect()->route('person.customers', ['model'=>$model])->with('success', $request["name"].' Created');
                }else if($person->personType == Config::get('constants.constants.supplier')){
                    return redirect()->route('person.supplier', ['model'=>$model])->with('success', $request["name"].' Created');
                }
            }else{
                return Redirect::back()->with('error', 'Error Occured');
            }
        }
        
        
    }


    public function edit($id)
    {
        $model = Person::find($id);

        if($model->personType == Config::get('constants.constants.customer')){
            return redirect()->route('person.customers', ['id'=>$model]);
        }else if($model->personType == Config::get('constants.constants.supplier')){
            return redirect()->route('person.supplier', ['id'=>$model]);
        }

        return "Contact With Adminstrator";
        
    }

    public function customers()
    {
        $model = new Person();
        $model->personType = Config::get('constants.constants.customer');
        return view('person.customers', ['model'=>$model]);
    }

    public function supplier()
    {
        $model = new Person();
        $model->personType = Config::get('constants.constants.supplier');
        return view('person.supplier', ['model'=>$model]);
    }
}
