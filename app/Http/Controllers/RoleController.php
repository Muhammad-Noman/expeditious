<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\roleHasPermissionHistory;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Role $role)
    {
        $this->middleware("auth");
        $this->role = $role;
    }
    public function index()
    {
        $roles= $this->role::all();
        return view('role.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
            
        ]);

        if(!empty($request->id)){
            $role = role::find($request->id);
            $role->name = $request["name"];
            $role->updated_at = date("Y-m-d h:i:s");
            if($role->save()){
                return redirect()->route('role.index')->with('success', 'Role Created');
            }else{
                return redirect()->route('role.index')->with('error', 'Error Occured');
            }

        }else{
            $role = $this->role->create([
                'name' => $request->name
            ]);
    
            return redirect()->route('role.index')->with('success', 'Role Created');
        }
        
        
    }

    public function getAll(){
        $roles = $this->role->all();
        return response()->json([
            'roles' => $roles
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = $this->role->all();

        $roleData = role::find($id);

        return view('role.index', ['roles' => $roles,'roleData'=> $roleData]);
            //->with('roleData', $roleData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $_GET;
        // var_dump($_GET);
        // $role = role::find($id);
        // $role->name = $request["name"];
        // var_dump($request);
        // if($role->save()){
        //     return redirect()->route('role.index')->with('success', 'Role Created');
        // }else{
        //     return redirect()->route('role.index')->with('error', 'Error Occured');
        // }
            echo "12345";
        
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    public function assignroles(){
        $users = User::all();       
        $role = Role::all();
        return view('role.assignrole',['users'=>$users, 'roles'=>$role]);
    } 
    
    public function assignRoleToUser(Request $request){
        
        if(isset($_POST['userid']) && $_POST['userid'] != ''){            
            $user = User::find($request->userid);
            if (count($user->roles->all()) > 0){
                $user->removeRole($user->roles->first());
            }            
            $user->assignRole($request->rolename);

            return redirect()->route('role.assignroles')->with('success', 'Invoice created successfully');    
       }        
        $users = User::all(); 
        
        $role = Role::all();
        return view('role.assignrole',['users'=>$users, 'roles'=>$role])>with('success', 'Role Assigned');    
    }

   public function assignpermission(Request $request){
       $role = Role::where('id', $request->id)->first();
       $permission = Permission::all();
    return view('role.assignpermission',['roles'=>$role, 'permissions'=>$permission]);    
   }   

   public function createpermission(){
       return view('role.createpermission');    
   }

   public function addpermission(Request $request){
     
        $permission = new Permission;
        $permission->name = $request->permission;
        $permission->save();

        return redirect()->route('role.createpermission')->with('success', 'Permission Created');
       
    }

    public function assignpermissiontorole(Request $request) {
        $name = Auth::user()->name;
        $permissionid = Permission::where('name', $request->permission)->first('id');

            $role = Role::find($request->roleid);
                      
            $role->givePermissionTo($request->permission);

           $permissionhistory = new roleHasPermissionHistory();
           $permissionhistory->permission_id = $permissionid->id;
           $permissionhistory->role_id = $request->roleid;
           $permissionhistory->createdBy = $name;
           $permissionhistory->save();

          return redirect()->route('role.assignpermission', ['id'=>$request->roleid])->with('success', 'Permission Assign To Role');

    }

    public function unassignpermissiontorole($roleid, $permissionid) {

        $role = Role::find($roleid);
        $role->revokePermissionTo($role->permissions->where('id', $permissionid));

        return 'Permission Unssign From Role';

    }

    


}