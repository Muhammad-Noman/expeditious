<?php


use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

function userHasPermission($permissionName){

    $user = User::find(auth()->user()->id);    
    if($user->hasRole('Super Admin')){
        return true;
    }
    
    $result = User::leftJoin('model_has_roles',"users.id","=", "model_has_roles.model_id")
    ->leftJoin("role_has_permissions","model_has_roles.role_id","=", "role_has_permissions.role_id")
    ->leftJoin("permissions","role_has_permissions.permission_id","=", "permissions.id")
    ->where(["users.id"=>auth()->user()->id,"permissions.name"=>$permissionName])->get();
    // dd($result);
    if (count($result)>0){
        return true;
    }
    return false;
}



?>