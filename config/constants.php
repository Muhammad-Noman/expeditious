<?php
return [
    'constants' => [
        'customer' => 'CUSTOMER',
        'supplier' => 'SUPPLIER',
        'salesInvoice'=>'SALES_INVOICE',
        'salesOrder'=>'SALES_ORDER',
        'purchaseOrder'=>'PURCHASE_ORDER',
        'purchaseInvoice'=>'PURCHASE_INVOICE',
        'rawProduct'=>'RAW_PRODUCT',
        'finishProduct'=>'FINISH_PRODUCT',
    ]
];
?>