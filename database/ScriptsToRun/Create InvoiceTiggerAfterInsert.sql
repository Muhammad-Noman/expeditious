CREATE TRIGGER InvoiceTiggerAffterInsert AFTER INSERT ON invoice
FOR EACH ROW
BEGIN
	set @documentId = NEW.invoice_id,
	@AccountId = New.invoice_customer_id * -1,
	@TransactionType = New.invoice_type,
	@debitAmount = 0, @creditAmount = 0;
	if (@TransactionType = 'SALES_INVOICE') then
		set @debitAmount = New.invoice_total_amount;
	else 
		if (@TransactionType = 'PURCHASE_INVOICE') then 
			set @creditAmount = New.invoice_total_amount;
		end if;
	end if;

	Insert into accounts_transactions (accounts_id,debit_amount,credit_amount,document_id,account_transaction_type,remarks,created_at,createdBy)values(@AccountId,@debitAmount,@creditAmount,@documentId,@TransactionType,'System Created',NOW(),'system');
END;
	