CREATE TRIGGER InvoiceTiggerAffterUpdate AFTER UPDATE ON invoice
FOR EACH ROW
BEGIN
	set @documentId = OLD.invoice_id,
	@AccountId = New.invoice_customer_id * -1,
	@TransactionType = New.invoice_type,
	@debitAmount = 0, @creditAmount = 0;
	if (@TransactionType = 'SALES_INVOICE') then
		set @debitAmount = New.invoice_total_amount;
	else 
		if (@TransactionType = 'PURCHASE_INVOICE') then 
			set @creditAmount = New.invoice_total_amount;
		end if;
	end if;

	UPDATE accounts_transactions set accounts_id = @AccountId, 
	debit_amount = @debitAmount,
	credit_amount=@creditAmount,
	updated_at = NOW(),createdBy = 'system' where document_id = @documentId;
END;
	