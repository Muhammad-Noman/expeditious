<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->integer('invoice_id');
            $table->string('invoice_document_id');
            $table->integer('invoice_parent_docuemt_id');
            $table->decimal('invoice_total_amount',11,2)->default(0.00);
            $table->decimal('invoice_disccount_amount',11,2)->default(0.00);
            $table->decimal('invoice_disccount_percentage',11,2)->default(0.00);
            $table->decimal('invoice_stax_amount',11,2)->default(0.00);
            $table->decimal('invoice_stax_percentage',11,2)->default(0.00);
            $table->decimal('invoice_additiontax_amount',11,2)->default(0.00);
            $table->decimal('invoice_additiontax_percentage',11,2)->default(0.00);
            $table->integer('invoice_customer_id');
            // $table->timestamps('invoice_date');
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->string('createdBy');
            $table->string('modifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
