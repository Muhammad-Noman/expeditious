<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->integer('person_id');
            $table->string('name');
            $table->date('dob')->nullable();
            $table->date('joining_date')->nullable();
            $table->string('contact_no');
            $table->string('contact_no_2')->nullable();
            $table->integer('parent_customer_id')->nullable();
            $table->string('address');
            $table->string('email');
            $table->integer('personType');
            $table->timestamps();
            $table->string('createdBy');
            $table->string('modifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
