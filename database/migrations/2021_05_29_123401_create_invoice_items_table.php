<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->integer('invoice_item_id');
            $table->integer('invoice_id');
            $table->integer('product_id');
            $table->decimal('invoice_item_price',11,2)->default(0.00);
            $table->decimal('invoice_item_qty',11,2)->default(0.00);
            $table->decimal('invoice_item_loose_qty',11,2)->default(0.00);
            $table->decimal('invoice_item_line_amount',11,2)->default(0.00);
            $table->decimal('invoice_item_disccount_amount',11,2)->default(0.00);
            $table->decimal('invoice_item_disccount_percentage',11,2)->default(0.00);
            $table->decimal('invoice_item_stax_amount',11,2)->default(0.00);
            $table->decimal('invoice_item_stax_percentage',11,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
