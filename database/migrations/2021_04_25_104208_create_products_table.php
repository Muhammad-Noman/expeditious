<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('salesPrice',11,2)->default(0.00);
            $table->decimal('purchasePrice',11,2)->default(0.00);
            $table->integer('parentProductId')->default(0);
            $table->integer('unitId')->default(0);
            $table->integer('categoryId')->default(0);
            $table->string('remarks')->nullable();            
            $table->timestamps();
            $table->string('createdBy');
            $table->string('modifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
