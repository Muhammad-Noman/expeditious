<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_flags', function (Blueprint $table) {
            $table->id();
            $table->string('flagName');
            $table->string('flagType');
            $table->integer('isActive')->default(1);
            $table->timestamps();
            $table->string('createdBy');
            $table->string('modifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_flags');
    }
}
