<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_transactions', function (Blueprint $table) {
            $table->integer('account_transaction_id');   
            $table->integer('accounts_id');            
            $table->decimal('debit_amount',20,2)->default(0.00);
            $table->decimal('credit_amount',20,2)->default(0.00);
            $table->integer('document_id');            
            $table->string('account_transaction_type');
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->string('createdBy');
            $table->string('modifiedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_transactions');
    }
}
