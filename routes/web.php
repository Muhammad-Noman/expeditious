<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

//Add Controller
Route::resource('user','UserController');
//Route::resource('permission','PermissionController');
Route::resource('role','RoleController');
Route::resource('products','ProductsController');
Route::resource('systemFlags','SystemFlagsController');
Route::resource('select2Search','Select2SearchController');
Route::resource('person','PersonController');
Route::resource('invoice','InvoiceController');
Route::resource('priceList','PriceListController');
Route::resource('accounts','AccountsController');

Route::get('/accountHead', 'AccountsController@accountHead')->middleware(['auth'])->name('accounts.accountHead');
Route::get('/PriceList', 'PriceListController@PriceList')->middleware(['auth'])->name('priceList.PriceList');
Route::get('/priceList/productRateUpdate/{id}/{price}', 'PriceListController@ProductRateUpdate')->middleware(['auth']);
Route::get('/salesInvoice', 'InvoiceController@salesInvoice')->middleware(['auth'])->name('invoice.salesInvoice');
Route::get('/SIASO', 'InvoiceController@SIASO')->middleware(['auth'])->name('invoice.SIASO');
Route::get('/salesOrder', 'InvoiceController@salesOrder')->middleware(['auth'])->name('invoice.salesOrder');
Route::get('/printSalesInvoice/{id}', 'InvoiceController@printSalesInvoice')->middleware(['auth'])->name('invoice.printSalesInvoice');

Route::get('/purchaseInvoice', 'InvoiceController@purchaseInvoice')->middleware(['auth'])->name('invoice.purchaseInvoice');
Route::get('/purchaseOrder', 'InvoiceController@purchaseOrder')->middleware(['auth'])->name('invoice.purchaseOrder');
Route::get('/PIAPO', 'InvoiceController@PIAPO')->middleware(['auth'])->name('invoice.PIAPO');

Route::get('/customers', 'PersonController@customers')->middleware(['auth'])->name('person.customers');
Route::get('/supplier', 'PersonController@supplier')->middleware(['auth'])->name('person.supplier');

Route::get('/products', 'ProductsController@products')->middleware(['auth'])->name('products.products');
Route::post('/storeProducts', 'ProductsController@storeProducts')->middleware(['auth'])->name('products.storeProducts');

Route::get('/unit', 'SystemFlagsController@unit')->middleware(['auth'])->name('systemFlags.unit');
Route::post('/storeUnit', 'SystemFlagsController@storeUnit')->middleware(['auth'])->name('systemFlags.storeUnit');

Route::get('/category', 'SystemFlagsController@category')->middleware(['auth'])->name('systemFlags.category');
Route::post('/storeCategory', 'SystemFlagsController@storeCategory')->middleware(['auth'])->name('systemFlags.storeCategory');

Route::get('/search', 'Select2SearchController@search')->middleware(['auth'])->name('select2Search.index');
Route::get('/ajax-autocomplete-search', 'Select2SearchController@selectSearch')->middleware(['auth'])->name('select2Search.selectSearch');

Route::get('/profile', 'UserController@profile')->middleware(['auth'])->name('user.profile');


Route::post('/profile', 'UserController@postProfile')->middleware(['auth'])->name('user.postProfile');
Route::post('/addprofile', 'UserController@addProfile')->middleware(['auth'])->name('user.addProfile');
Route::get('/registration', 'UserController@userRegistration')->middleware(['auth'])->name('user.userRegistration');


Route::post('/assignRoleToUser', 'RoleController@assignRoleToUser')->middleware(['auth'])->name('role.assignRoleToUser');
Route::get('/assignroles', 'RoleController@assignroles')->middleware(['auth'])->name('role.assignroles');
Route::post('/addassignroles', 'RoleController@addassignroles')->middleware(['auth'])->name('role.addassignroles');
Route::get('/assignpermission', 'RoleController@assignpermission')->middleware(['auth'])->name('role.assignpermission');
Route::post('/addpermission', 'RoleController@addpermission')->middleware(['auth'])->name('role.addpermission');
Route::get('/createpermission', 'RoleController@createpermission')->middleware(['auth'])->name('role.createpermission');
Route::post('/assignpermissiontorole', 'RoleController@assignpermissiontorole')->middleware(['auth'])->name('role.assignpermissiontorole');
Route::get('/unassignpermissiontorole/{roleid}/{permissionid}', 'RoleController@unassignpermissiontorole')->middleware(['auth'])->name('role.unassignpermissiontorole');

// Route::post('/password/change', 'UserController@postPassword')->middleware(['auth'])->name('userPostPassword');



require __DIR__.'/auth.php';
