<?php 
use App\Models\SystemFlags;
use App\Models\Person;
if(isset($_GET['id']) && $_GET['id'] != ''){
      $model = Person::find($_GET['id']);
}
?>
@extends('master')

@section('title')
Supplier
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Supplier</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="col-lg-12">
              <div class="card"> 
                <div class="card-header">
                  <h3 class="card-title">Add</h3>
      
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                  </div>
                </div>
                 {{--Card header End  --}}
                <form method="POST" action="{{ route('person.store') }}">
                  @csrf
                  <div class="card-body">
                      
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="id">Id</label>
                            <input type="text" name="person_id"  id="person_id" class="form-control @error('person_id') is-invalid @enderror" value="{{  $model->person_id }}" readonly>
                            @error('person_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
    
                            
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="name">Name</label>
                              <input type="text" name="name"  id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $model->name }}">
                              @error('id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                        </div>
                      </div>
                    {{-- //////////////////// --}}
                      <div class="row">

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="name">Contact No :</label>
                              <input type="text" name="contact_no"  id="contact_no" class="form-control @error('contact_no') is-invalid @enderror" value="{{ $model->contact_no }}" required>
                              @error('contact_no')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>

                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="name">Contact No 2 :</label>
                              <input type="text" name="contact_no_2"  id="contact_no_2" class="form-control @error('contact_no_2') is-invalid @enderror" value="{{ $model->contact_no_2 }}">
                              @error('contact_no_2')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>
                           
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="name">Email :</label>
                              <input type="email" name="email"  id="email" class="form-control @error('email') is-invalid @enderror" value="{{ $model->email }}">
                              @error('email')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="name">Address :</label>
                              <textarea name="address"  id="address" class="form-control" rows="3" placeholder="Enter ..."                  
                              @error('address') is-invalid @enderror" >{{ $model->address }}</textarea>
                              @error('address')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="parent_customer_id">Parent Supplier</label><br>
                              <select type="text" name="parent_customer_id"  id="parent_customer_id" class="form-control select2bs4 @error('parent_customer_id') is-invalid @enderror" 
                                      placeholder="Select Parent (Optional)" autocomplete="off">
                                      <option value='' >Select Parent Product (Optional)</option>
                                      @foreach (Person::where('personType',$model->personType)->where("person_id",'<>',$model->person_id)->get() as $person)
                                        <option {{ ($model->parent_customer_id == $person->person_id) ? "selected" : "" }} value="{{ $person->person_id }}">{{ $person->name }}</option>                                  
                                  @endforeach
                              </select>
                              @error('parent_customer_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          </div>

                        </div>


                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="hidden" name="personType"  id="personType" class="form-control @error('personType') is-invalid @enderror" value="{{ $model->personType }}">
                              @error('personType')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>

                        </div>

                        
                  </div>
                  
                  <div class="card-footer">
                  @if(userHasPermission('Add Supplier'))
                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{$model->person_id == '' ? ' Create' : " Update"}}</button>
                  @endif
                    </div>
                </form>  
              </div>  
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="personDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Contact No</th>
                              <th>Email</th>
                              <th>Parent Supplier</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (Person::where('personType',$model->personType)->get() as $person)
                              <tr>
                                  <td>{{ $person->person_id }}</td>
                                  <td>{{ $person->name }}</td>
                                  <td>{{ $person->contact_no }}</td>
                                  <td>{{ $person->email }}</td>
                                  <td>{{ Person::getPersonName($person->parent_customer_id) }}</td>
                                  <td>
                                  @if(userHasPermission('Edit Supplier'))
                                      <a href="{{ route('person.edit', $person->person_id) }}" class="btn btn-sm btn-warning">Edit</a>
                                  @endif
                                  @if(userHasPermission('Delete Supplier'))
                                      <a href="{{ route('person.destroy', $person->person_id) }}" class="btn btn-sm btn-danger">Delete</a>
                                  @endif 
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $('#parentProductId').select2();
  $('#categoryId').select2();
  $('#unitId').select2();
  $(function () {
    $('#personDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection()