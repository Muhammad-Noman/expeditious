<?php 
use App\Models\SystemFlags;
use App\Models\Products;
use App\Models\Invoice;
use App\Models\Person;
use App\Models\InvoiceItem;

if(isset($_GET['OrderId']) && $_GET['OrderId'] != ''){
      $model = Invoice::find($_GET['OrderId']);
      $invoiceItem = InvoiceItem::where('invoice_id',$_GET['OrderId'])->get();

}
$counter = 0;
?>

@extends('master')

@section('title')
Purchase Order
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
              <div class="card"> 
                <div class="card-header">
                  <h3 class="card-title">Add</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                 {{--Card header End  --}}
                 <div class="card-body">
                <form method="POST" action="{{ route('invoice.store') }}">
                  @csrf 
                    <div class="row">                      
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="hidden" name="invoice_type"  id="invoice_type" class="form-control @error('invoice_type') is-invalid @enderror" value="{{  $model->invoice_type }}" readonly>
                          @error('invoice_type')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
  
                          
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <input type="hidden" name="invoice_id"  id="invoice_id" class="form-control @error('invoice_id') is-invalid @enderror" value="{{  $model->invoice_id??'' }}" readonly>
                          @error('invoice_id')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
  
                          
                        </div>
                      </div>
                    </div>
                    <div class="row">                          
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="invoice_date">Order Date</label><br>
                          <input type="date" name="invoice_date"  id="invoice_date" class="form-control @error('invoice_date') is-invalid @enderror" value="<?php echo date('Y-m-d'); ?>">
                            @error('invoice_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="invoice_customer_id">Customer #</label><br>
                          <select type="text" name="invoice_customer_id"  id="invoice_customer_id" class="form-control select2bs4 @error('invoice_customer_id') is-invalid @enderror" 
                                placeholder="Select Customer" autocomplete="off">
                                <option value="" >Select Customer</option>
                                @foreach (Person::where('personType',Config::get('constants.constants.customer'))->get() as $Customer)
                                  <option {{ $model->invoice_customer_id == $Customer->person_id ? "selected" : "" }} value="{{ $Customer->person_id }}">{{ $Customer->name }}</option>                                  
                            @endforeach
                        </select>
                          @error('invoice_customer_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                        </div>
                      </div>
                    
                    </div>
                    {{-- End of First Row --}}
                    <div class="row">
                      </div>
                    {{-- End of Second Row --}}
                  
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="remarks">Remarks :</label><br>
                          <textarea name="remarks"  id="remarks" class="form-control" rows="3" placeholder="Optional ..."
                          @error('remarks') is-invalid @enderror" value="{{  $model->remarks }}" ></textarea>
                          @error('remarks')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                        </div>
                      </div>
                    </div>
                    {{-- End of Third Row --}}
                    <div class="row">
                          <div class="col-md-12">
                            <div class="card">
                              <!-- /.card-header -->
                              <div class="card-body">
                                <table class="table table-bordered" id='invoiceItems' style="overflow-x: auto">
                                  <thead>                  
                                    <tr>
                                      <th style="width: 10px">#</th>
                                      <th>Product</th>
                                      <th>Price</th>
                                      <th>Qty</th>
                                      <th>Discount %</th>
                                      <th>Disccount Amt</th>
                                      <th>Final Amt</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (isset($_GET['OrderId']) && $_GET['OrderId'] != '')
                                        @foreach ($invoiceItem as $item)
                                        <tr id="invoiceItem-<?php echo $counter;?>">
                                          <td id="invoiceItem-<?php echo $counter;?>-sr"><?php echo $counter+1;?></td>
                                          <td style="display:none">
                                            <input type="number" name="invoiceItem[<?php echo $counter;?>][invoice_item_id]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_id" min='0' class="form-control" value="{{ $item->invoice_item_id??'' }}">
                                          </td>
                                          <td width="25%">
                                            <select  type="text" name="invoiceItem[<?php echo $counter;?>][product_id]"  id="invoiceItem-<?php echo $counter;?>-product_id" class="form-control select2bs4 select2" 
                                              placeholder="Enter Product" autocomplete="off">
                                              <option value='' >Enter Product</option>
                                              @foreach (Products::where('productType',Config::get('constants.constants.finishProduct'))->get() as $products)
                                                <option {{ ($item->product_id == $products->id) ? "selected" : "" }} value="{{ $products->id }}">{{ $products->name }}</option>                                  
                                              @endforeach
                                            </select>
                                          </td>
                                          <td>
                                            <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_price]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_price" min='0' class="form-control" value="{{ $item->invoice_item_price??0 }}">
                                          </td>
                                          <td>
                                            <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_qty]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_qty" class="form-control" min='0' value="{{ $item->invoice_item_qty??0}}">
                                          </td>
                                          <td >
                                            <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_disccount_percentage]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_disccount_percentage" class="form-control" min='0' value="{{ $item->invoice_item_disccount_percentage??0}}">
                                          </td>
                                          <td>
                                            <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_disccount_amount]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_disccount_amount" class="form-control" readonly min='0' value="{{ $item->invoice_item_disccount_amount??0}}">
                                          </td>                                      
                                          <td width="15%">
                                            <input type="number" name="invoiceItem[<?php echo $counter;?>][invoice_item_line_amount]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_line_amount" class="form-control" min='0' value="{{$item->invoice_item_line_amount??0}}" readonly>
                                          </td>
                                        </tr>  
                                        @php $counter = $counter+1;  @endphp
                                        @endforeach
                                    @endif
                                        
                                      <tr id="invoiceItem-<?php echo $counter;?>">
                                        <td id="invoiceItem-<?php echo $counter;?>-sr"><?php echo $counter+1;?></td>
                                        <td style="display:none">
                                          <input type="number" name="invoiceItem[<?php echo $counter;?>][invoice_item_id]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_id" min='0' class="form-control" >
                                        </td>
                                        <td width="25%">
                                          <select  type="text" name="invoiceItem[<?php echo $counter;?>][product_id]"  id="invoiceItem-<?php echo $counter;?>-product_id" class="form-control select2bs4 select2" 
                                            placeholder="Enter Product" autocomplete="off">
                                            <option value='' >Enter Product</option>
                                            @foreach (Products::where('productType',Config::get('constants.constants.finishProduct'))->get() as $products)
                                              <option value="{{ $products->id }}">{{ $products->name }}</option>                                  
                                            @endforeach
                                          </select>
                                        </td>
                                        <td>
                                          <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_price]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_price" min='0' class="form-control" value="0">
                                        </td>
                                        <td>
                                          <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_qty]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_qty" class="form-control" min='0' value="0">
                                        </td>
                                        <td >
                                          <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_disccount_percentage]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_disccount_percentage" class="form-control" min='0' value="0">
                                        </td>
                                        <td>
                                          <input type="number" onblur="calculateLineAmount(this);" name="invoiceItem[<?php echo $counter;?>][invoice_item_disccount_amount]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_disccount_amount" class="form-control" readonly min='0' value="0">
                                        </td>                                      
                                        <td width="15%">
                                          <input type="number" name="invoiceItem[<?php echo $counter;?>][invoice_item_line_amount]"  id="invoiceItem-<?php echo $counter;?>-invoice_item_line_amount" class="form-control" min='0' value="0" readonly>
                                        </td>
                                      </tr>                                      
                                  </tbody>
                                </table>
                              </div>
                              <!-- /.card-body -->                              
                            </div>
                            <!-- /.card -->
                {{-- End of Product Table  --}}
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      </div>
                    </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="invoice_exclusive_amount">Exlcusive Amount :</label>
                      <input type="number" name="invoice_exclusive_amount"  id="invoice_exclusive_amount" readonly
                       class="form-control @error('invoice_exclusive_amount') is-invalid @enderror" value="{{  $model->invoice_exclusive_amount }}">
                      @error('invoice_exclusive_amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <br>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="invoice_disccount_percentage">Discount % :</label>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="number" name="invoice_disccount_percentage"  id="invoice_disccount_percentage" 
                              class="form-control @error('invoice_disccount_percentage') is-invalid @enderror" value="{{  $model->invoice_disccount_percentage }}" onblur="calculateAllLineAmout();">
                              @error('invoice_disccount_percentage')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                              
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="number" name="invoice_disccount_amount"  id="invoice_disccount_amount" readonly
                              class="form-control @error('invoice_disccount_amount') is-invalid @enderror" value="{{  $model->invoice_disccount_amount }}"> 
                              @error('invoice_disccount_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          </div>
      
                          </div>
                          {{-- Discount row --}}
                          <br>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="invoice_stax_percentage">STax % :</label>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="number" name="invoice_stax_percentage"  id="invoice_stax_percentage" 
                              class="form-control @error('invoice_stax_percentage') is-invalid @enderror" value="{{  $model->invoice_stax_percentage }}"
                              onblur="calculateAllLineAmout();">
                              @error('invoice_stax_percentage')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                              
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <input type="number" name="invoice_stax_amount"  id="invoice_stax_amount" readonly
                              class="form-control @error('invoice_stax_amount') is-invalid @enderror" value="{{  $model->invoice_stax_amount }}"> 
                              @error('invoice_stax_amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          </div>
      
                          </div>
                      
                      {{-- Stax Row --}}
                      <br>
                      <label for="invoice_total_amount">Inclusive Amount :</label>
                      <input type="number" name="invoice_total_amount"  id="invoice_total_amount" readonly
                       class="form-control @error('invoice_total_amount') is-invalid @enderror" value="{{  $model->invoice_total_amount }}">
                      @error('invoice_total_amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      
    
                    </div>
                  </div>
                </div>
                {{-- End of Fourth Row --}}
                  </div>
                  
                  <div class="card-footer">
                  @if(userHasPermission('Add Purchase Order'))
                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{$model->invoice_id == '' ? ' Create' : " Update"}}</button>
                  @endif 
                    </div>
                </form>  
                 </div>
              </div>  
            </div>
            <div class="card">         
              <div class="card-body table-responsive p-0">
                  <table id="invoiceDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>Document Id</th>
                              <th>Customer</th>
                              <th>Order Date </th>
                              <th>Order Amt</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (Invoice::where('invoice_type',$model->invoice_type)->get() as $inovice)
                              <tr> 
                                  <td>{{ $inovice->invoice_document_id }}</td>
                                  <td>{{ Person::getPersonName($inovice->invoice_customer_id) }}</td>
                                  <td>{{ $inovice->invoice_date }}</td>
                                  <td>{{ $inovice->invoice_total_amount }}</td><td>
                                  @if(userHasPermission('Make PI Purchase Order'))
                                      <a href="{{ route('invoice.PIAPO', ['id' => $inovice->invoice_id]) }}" class="btn btn-sm btn-success">Make PI</a>
                                  @endif
                                  @if(userHasPermission('Print Purchase Order'))
                                  <a href="{{ route('invoice.show', $inovice->invoice_id) }}" class="btn btn-sm btn-primary">Print</a>
                                  @endif
                                  @if(userHasPermission('Edit Purchase Order'))
                                  <a href="{{ route('invoice.purchaseOrder',['OrderId' => $inovice->invoice_id]) }}" class="btn btn-sm btn-warning">Edit</a>
                                  @endif
                                  @if(userHasPermission('Delete Purchase Order'))
                                  <a onclick="return confirm('Are you sure?')"  href="{{ route('invoice.destroy', $inovice->invoice_id) }}" class="btn btn-sm btn-danger">Delete</a>                                      
                                  @endif
                                </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div></div>    
            
        </div>
        <!-- /.row -->
       
        <!-- /.card-body -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $('#invoice_customer_id').select2();
  $('#invoiceItem-0-product_id').select2();

  function calculateLineAmount(obj){
    rowId = obj.id.split('-')[1];
    var productAmt = parseFloat($('#invoiceItem-'+rowId+'-invoice_item_price').val()).toFixed(2);
    var productQty = parseFloat($('#invoiceItem-'+rowId+'-invoice_item_qty').val()).toFixed(2);
    var productDisPer = parseFloat($('#invoiceItem-'+rowId+'-invoice_item_disccount_percentage').val()).toFixed(2);    

    var productTotalAmt = productAmt * productQty;
    var productDisAmt = productTotalAmt * (productDisPer/100);
    
    var lineAmt = productTotalAmt-productDisAmt;

    $('#invoiceItem-'+rowId+'-invoice_item_disccount_amount').val(productDisAmt);
    
    $('#invoiceItem-'+rowId+'-invoice_item_line_amount').val(lineAmt);
    

    checkRow(rowId);

    calculateAllLineAmout();

  }

  function calculateAllLineAmout(){
    var sum = parseInt(0);
    
    $('#invoiceItems > tbody > tr').each(function (i,row){

      var $row = $(row);
      var rowlineAmt = Number($('#invoiceItem-'+i+'-invoice_item_line_amount').val()); 
      
      sum = sum + rowlineAmt;

    });
    $('#invoice_exclusive_amount').val(parseFloat(sum).toFixed(2));

    var invoiceStaxPer = Number($('#invoice_stax_percentage').val());
    var invoiceDisPer = Number($('#invoice_disccount_percentage').val());
    var invoiceDisAmt = sum * (invoiceDisPer/100);
    var invoiceStaxAmt = (sum-invoiceDisAmt) * (invoiceStaxPer/100);

    var invoiceTotalAmt = (sum-invoiceDisAmt) + invoiceStaxAmt;

    $('#invoice_disccount_amount').val(parseFloat(invoiceDisAmt).toFixed(2));
    $('#invoice_stax_amount').val(parseFloat(invoiceStaxAmt).toFixed(2));
    $('#invoice_total_amount').val(parseFloat(invoiceTotalAmt).toFixed(2));




    
  }

  function checkRow(index){
    
    var rowCount = $('#invoiceItems tr').length;
    if(rowCount-2 == index){
      var newId = parseInt(index)+1
      $table = $("#invoiceItems");
      $row = $("#invoiceItem-"+(index)); 
      $('#invoiceItem-'+index+'-product_id').select2('destroy');
      $clone = $row.clone(true); 
      $clone.attr ('id', "invoiceItem-"+newId);
      $clone.find('#invoiceItem-'+index+'-sr').html(newId);
      $clone.find('#invoiceItem-'+index+'-sr').attr('id', 'invoiceItem-'+newId+'-sr');
      $clone.find('#invoiceItem-'+index+'-product_id').attr({"name":'invoiceItem['+newId+'][product_id]',
              'id':'invoiceItem-'+newId+'-product_id'}).val('');
      $clone.find('#invoiceItem-'+index+'-invoice_item_price').attr({'id':'invoiceItem-'+newId+'-invoice_item_price',
              'name':'invoiceItem['+newId+'][invoice_item_price]'}).val(0);
      $clone.find('#invoiceItem-'+index+'-invoice_item_qty').attr({'id':'invoiceItem-'+newId+'-invoice_item_qty',
              "name":'invoiceItem['+newId+'][invoice_item_qty]'}).val(0);
      $clone.find('#invoiceItem-'+index+'-invoice_item_disccount_percentage').attr({'id':'invoiceItem-'+newId+'-invoice_item_disccount_percentage',
              "name":'invoiceItem['+newId+'][invoice_item_disccount_percentage]'}).val(0);
      $clone.find('#invoiceItem-'+index+'-invoice_item_disccount_amount').attr({'id':'invoiceItem-'+newId+'-invoice_item_disccount_amount',
              "name":'invoiceItem['+newId+'][invoice_item_disccount_amount]'}).val(0);
      $clone.find('#invoiceItem-'+index+'-invoice_item_stax_percentage').attr({'id':'invoiceItem-'+newId+'-invoice_item_stax_percentage',
              'name':'invoiceItem['+newId+'][invoice_item_stax_percentage]'}).val(0);              
      $clone.find('#invoiceItem-'+index+'-invoice_item_stax_amount').attr({'id':'invoiceItem-'+newId+'-invoice_item_stax_amount',
              'name':'invoiceItem['+newId+'][invoice_item_stax_amount]'}).val(0);
      $clone.find('#invoiceItem-'+index+'-invoice_item_line_amount').attr({'id':'invoiceItem-'+newId+'-invoice_item_line_amount',
              'name':'invoiceItem['+newId+'][invoice_item_line_amount]'}).val(0);
      $table.append($clone); 
      //below code for select 2 change
      $('#invoiceItem-'+index+'-product_id').select2();
      $('#invoiceItem-'+newId+'-product_id').select2();
      $('#invoiceItem-'+newId+'-sr').html(newId+1);
    }else{

    }
  }

  $(function () {
    // $('#invoice_date').val(new Date().toDateInputValue());
    $('#invoiceDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });
</script>
@endsection()