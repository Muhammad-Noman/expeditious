<?php 
use App\Models\SystemFlags;
use App\Models\Products;
use App\Models\Person;
use App\Models\PriceList;

$products = Products::whereNotIn('id', function($query){
    $query->select('product_id')
    ->from('price_lists');
})->get();
$priceListProduct = PriceList::all();
?>
@extends('master')

@section('title')
Price List
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Price List</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
              <div class="card"> 
                <div class="card-header">
                  <h3 class="card-title">Update Price List</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                 {{--Card header End  --}}
                 <div class="card-body">
                  <div class="card-body table-responsive p-0">
                    <table id="priceListDataTable" class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Sales Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($products as $product)
                                <tr>
                                    <td id="name-{{ $product->id }}">{{ $product->name }}</td>
                                    <td><input type="number" id="productID-{{ $product->id }}"  min="0" value="" ></td>
                                    <td>
                                        <input type="button" value="Update Price" onclick="UpdatePrice({{ $product->id }});">
                                    </td>
                                </tr>
                            @empty
                                
                            @endforelse
                            @forelse ($priceListProduct as $product)
                                <tr>
                                    <td id="name-{{ $product->id }}">{{ $product->ProductName->name }}</td>
                                    <td><input type="number" id="productID-{{ $product->id }}"  min="0" value="{{$product->salesPrice}}" ></td>
                                    <td>
                                    @if(userHasPermission('Update Price List'))
                                        <input type="button" value="Update Price" onclick="UpdatePrice({{ $product->id }});">
                                    @endif
                                      </td>
                                </tr>
                            @empty
                                
                            @endforelse
                            
                        </tbody>
                    </table>
                </div>
                 </div>
              </div>  
            </div>
        </div>
        <!-- /.row -->
       
        <!-- /.card-body -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>

var price;

// $(document).on("change","#price",function() {
//      price = $(this).val();
// });


  $('#invoice_customer_id').select2();
  $('#invoiceItem-0-product_id').select2();

  function UpdatePrice(id){
    price = $('#productID-'+id).val();
    productName = $('#name-'+id).html();
    bootbox.confirm("Want to update "+productName+"?", function(result) {
      $.ajax({        
        url: "/priceList/productRateUpdate/" + id + "/" + price
      })
      .done(function( data ) {
        bootbox.alert(data);
        });
      });
  }

  $(function () {
    // $('#invoice_date').val(new Date().toDateInputValue());
    $('#invoiceDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });
</script>
@endsection()