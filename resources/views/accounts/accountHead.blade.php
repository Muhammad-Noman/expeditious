<?php 
use App\Models\SystemFlags;
use App\Models\Accounts;
if(isset($_GET['id']) && $_GET['id'] != ''){
      $model = Accounts::find($_GET['id']);
}
?>
@extends('master')

@section('title')
Accounts Head
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Accounts Head</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="col-lg-12">
              <div class="card"> 
                <div class="card-header">
                  <h3 class="card-title">Add</h3>
      
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                  </div>
                </div>
                 {{--Card header End  --}}
                <form method="POST" action="{{ route('accounts.store') }}">
                  @csrf
                  <div class="card-body">
                      
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="id">Id</label>
                            <input type="text" name="account_id"  id="account_id" class="form-control @error('account_id') is-invalid @enderror" value="{{  $model->account_id }}" readonly>
                            @error('account_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
    
                            
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="account_name">Name</label>
                              <input type="text" name="account_name"  id="account_name" class="form-control @error('account_name') is-invalid @enderror" value="{{ $model->account_name }}">
                              @error('account_name')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                        </div>
                      </div>
                    {{-- //////////////////// --}}
    
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="parent_account_id">Parent Customer</label><br>
                              <select type="text" name="parent_account_id"  id="parent_account_id" class="form-control select2bs4 @error('parent_account_id') is-invalid @enderror" 
                                      placeholder="Select Parent (Optional)" autocomplete="off">
                                      <option value='' >Select Parent Product (Optional)</option>
                                      @foreach (Accounts::where('account_level',3)->where("account_id",'<>',$model->account_id)->get() as $account)
                                        <option {{ ($model->parent_account_id == $account->account_id) ? "selected" : "" }} value="{{ $account->account_id }}">{{ $account->name }}</option>                                  
                                  @endforeach
                              </select>
                              @error('parent_account_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <input type="hidden" name="account_level"  id="account_level" class="form-control @error('account_level') is-invalid @enderror" value="{{ $model->account_level }}">
                              @error('account_level')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                          </div>

                        </div>

                        
                  </div>
                  
                  <div class="card-footer">
                  @if(userHasPermission('Add Account Head'))
                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{$model->account_id == '' ? ' Create' : " Update"}}</button>
                  @endif
                    </div>
                </form>  
              </div>  
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="personDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Parent Account</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (Accounts::where('account_level',$model->account_level)->get() as $account)
                              <tr>
                                  <td>{{ $account->account_id }}</td>
                                  <td>{{ $account->account_name }}</td>
                                  <td>{{ Accounts::getAccountName($account->parent_account_id) }}</td>
                                  <td>
                                  @if(userHasPermission('Edit Account Head'))
                                      <a href="{{ route('accounts.edit', $account->account_id) }}" class="btn btn-sm btn-warning">Edit</a>
                                  @endif
                                  @if(userHasPermission('Delete Account Head'))
                                      <a href="{{ route('accounts.destroy', $account->account_id) }}" class="btn btn-sm btn-danger">Delete</a>
                                  @endif 
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $('#parent_account_Id').select2();
  $(function () {
    $('#personDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection()