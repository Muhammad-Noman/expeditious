@extends('master')

@section('title')
  Permission
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Permission</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card">  
              <form method="POST" action="{{ route('permission.store') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Permission Name</label>
                        <input type="text" name="name"  id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required placeholder="Permission Name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Create Permission</button>
                </div>
              </form>
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="permissionDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Date Posted</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($permissions as $permission)
                              <tr>
                                  <td>{{ $permission->id }}</td>
                                  <td>{{ $permission->name }}</td>
                                  <td>{{ $permission->created_at }}</td>
                                  <td>
                                      <a href="{{ route('permission.edit', $permission->id) }}" class="btn btn-sm btn-warning">Edit Permission</a>
                                  </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $(function () {
    $('#permissionDataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection()