<?php 
use App\Models\SystemFlags;
use App\Models\Products;
use App\Models\Person;
use App\Models\PriceList;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
?>
@extends('master')

@section('title')
Create Permission 
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Permission </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                 <form method="POST" action="{{route('role.addpermission')}}">  
                    @csrf
                        <label>
                                <b> Permission Name  </b>
                        </label>
                        <br>
                           <div class="row">
                              <div class="col-md-4">
                               <input type="text" name="permission" class="form-control" placeholder="Permission Name" />
                              </div>
                           </div> 
                        <br>
                      <div class="mb-3">
                           <input type="submit" class="btn btn-primary" value="Create Permission" />
                      </div>  
                </div>
                </form>
              </div>  
                         <br>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="productsDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (Permission::all() as $permission)
                              <tr>
                                  <td>{{ $permission->id }}</td>
                                  <td>{{ $permission->name }}</td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.row -->
       
        <!-- /.card-body -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script>
    

$('.selectpicker').selectpicker();


  $(function () {
    // $('#invoice_date').val(new Date().toDateInputValue());
    $('#invoiceDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });

</script>
@endsection()