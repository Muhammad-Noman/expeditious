<?php 
use App\Models\SystemFlags;
use App\Models\Products;
use App\Models\Person;
use App\Models\PriceList;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\roleHasPermission;
?>
@extends('master')

@section('title')
Permission Roles
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Permission Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                 <form method="POST" action="{{route('role.assignpermissiontorole')}}">  
                    @csrf
                    <div class="col-md-4">
                    <input type="hidden" class="form-control" id="roleid" name="roleid" value="{{$roles->id}}" readonly />                    
                     <div class="mb-3">
                       <label class="form-label">Role</label> <br>
                       <input type="text" class="form-control" id="role" name="role" value="{{$roles->name}}" readonly />                    
                      </div>       
                        <label>
                                <b>   Permission   </b>
                        </label>
                        <br>
                       <div class="product-show">
                               <select class="form-control" name="permission" >
                                 @foreach($permissions as $permission)
                                <option value="{{$permission->name}}">{{$permission->name}}</option>
                                 @endforeach 
                               </select>
                       </div> <br>
                      <div class="mb-3">
                           <input type="submit" class="btn btn-primary" value="Assign Permission" />
                      </div>  
                </div>
                </form>
              </div>  
            </div>
            </div>  
        </div>
        <!-- /.row -->

        <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="productsDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Role</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (roleHasPermission::all() as $permission)
                            @if($roles->id == $permission->RoleName->id )
                              <tr>
                                  <td>{{ $permission->PermissionName->name }}</td>
                                  <td>{{ $permission->RoleName->name }}</td>
                                  <td>
                                  <input type="button" value="Remove" class="btn btn-sm btn-danger" onclick="unassignpermissionfromrole({{ $permission->RoleName->id }}, {{$permission->PermissionName->id }});">
                                </td>
                              </tr>
                             @endif 
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
       
        <!-- /.card-body -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')

<script>

function unassignpermissionfromrole(roleid, permissionid){
     
    bootbox.confirm("Want to unasign permission form role", function(result) {
      $.ajax({        
        url: "/unassignpermissiontorole/" + roleid + "/" + permissionid
      })
      .done(function( data ) {
        bootbox.alert(data);
        });
        location.reload();
      });
  }

 

  $(function () {
    // $('#invoice_date').val(new Date().toDateInputValue());
    $('#invoiceDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });

</script>
@endsection()