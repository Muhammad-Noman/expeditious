<?php 
use App\Models\SystemFlags;
use App\Models\Products;
use App\Models\Person;
use App\Models\PriceList;
use App\Models\User;
use App\Models\userHasRoles;
use Spatie\Permission\Models\Role;

$userid = $_POST['user']??'';
$roleid = $_POST['role']??'';
?>
@extends('master')

@section('title')
Assign Roles
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Assign Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                    <form method="POST" action="{{route('role.assignRoleToUser')}}">  
                    @csrf
                    <div class="col-md-4">  
                     <div class="mb-3">
                       <label class="form-label">Users</label> <br>
                       <select class="form-control" id="userid" name="userid"
                       <?php echo ($userid != '' ? "readonly":""); ?>>
                        <option >Select User</option>
                        @foreach($users as $user)
                         <option value="{{$user->id}}" <?php echo ($userid == $user->id? "selected":""); ?>>{{$user->name}}</option>
                        @endforeach
                      </select>                   
                      </div>       
                      <div class="mb-3">
                       <label class="form-label">Roles</label> <br>
                       <select class="form-control" id="rolename" name="rolename">
                         <option>Select Role</option>
                         @foreach($roles as $role)
                          <option value="{{$role->name}}" <?php echo ($roleid == $role->id? "selected":""); ?>>{{$role->name}}</option>
                         @endforeach
                       </select>                     
                      </div> 
                      <div class="mb-3">
                           <input type="submit" class="btn btn-primary" value="Assign Role" />
                      </div>  
                </div>
                </form>
              </div>  
            </div>

            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="productsDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Role</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (userHasRoles::all() as $user)
                              <tr>
                                  <td>{{ $user->UserName->name }}</td>
                                  <td>{{ $user->RoleName->name }}</td>
                                  <td>
                                      <a onclick="fillUserAndRole({{$user->role_id.','.$user->model_id}});" class="btn btn-sm btn-warning">Edit</a>
                                  </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
       
        <!-- /.card-body -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>

function fillUserAndRole(roleId,userId){
  var roleId = roleId;//$('#rolename').val();
  var userId = userId;//$('#userid').val();
  var form = $('<form action="/assignRoleToUser" method="post">@csrf' +
  '<input type="text" name="role" value="' + roleId + '" />' +
  '<input type="text" name="user" value="' + userId + '" />' +
  '</form>');
  $('body').append(form);
  form.submit();
  // window.location.href = "/assignRoleToUser?role="+roleId+"&user="+userId;
}

  $(function () {
    // $('#invoice_date').val(new Date().toDateInputValue());
    $('#invoiceDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
    });
  });

</script>
@endsection()