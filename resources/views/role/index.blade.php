@extends('master')

@section('title')
Roles
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card">  
            @if(isset($roleData) )
            <form method="POST" action="{{ route('role.store') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="name">Role Id</label>
                        <input type="text" name="id"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ $roleData->id }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" name="name"  id="name" class="form-control @error('name') is-invalid @enderror" 
                                value="{{ $roleData->name }}" required placeholder="Role Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Update Roles</button>
                </div>
              </form>
            @else
            <form method="POST" action="{{ route('role.store') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="name">Role Id</label>
                        <input type="text" name="id"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ old('id') }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" name="name"  id="name" class="form-control @error('name') is-invalid @enderror" 
                              value="{{ old('name') }}" required placeholder="Role Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Create Roles</button>
                </div>
              </form>
            @endif
              
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="rolesDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Roles</th>
                              <th>Date Posted</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($roles as $role)
                              <tr>
                                  <td>{{ $role->id }}</td>
                                  <td>{{ $role->name }}</td>
                                  <td>{{ $role->created_at }}</td>
                                  <td>
                                      <a href="{{ route('role.edit', $role->id) }}" class="btn btn-sm btn-warning">Edit Role</a>
                                      <a href="{{ route('role.assignpermission', ['id'=>$role->id]) }}" class="btn btn-sm btn-success">Permission</a>
                                     
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $(function () {
    $('#rolesDataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection()