<?php 
use App\Models\SystemFlags;
use App\Models\Products;
?>
@extends('master')

@section('title')
Products
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="col-lg-12">
              <div class="card"> 
                <div class="card-header">
                  <h3 class="card-title">Add</h3>
      
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                  </div>
                </div>
                 {{--Card header End  --}}
                <form method="POST" action="{{ route('products.store') }}">
                  @csrf
                  <div class="card-body">
                    <div style = 'display:none' class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="id">Product Type</label>
                              <input type="text" name="productType"  id="productType" class="form-control @error('productType') is-invalid @enderror" value="{{  $model->productType }}" readonly>
                              @error('productType')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
      
                              
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="id">Id</label>
                              <input type="text" name="id"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{  $model->id }}" readonly>
                              @error('id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
      
                              
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                              <label for="categoryId">Categoery</label><br>
                            <select type="text" name="categoryId"  id="categoryId" class="form-control select2bs4 @error('categoryId') is-invalid @enderror" 
                                placeholder="Select Category" autocomplete="off">
                                <option value="" >Select Category</option>
                                @foreach (SystemFlags::where('flagType','CATEGORY')->get() as $Category)
                                  <option {{ $model->categoryId == $Category->id ? "selected" : "" }} value="{{ $Category->id }}">{{ $Category->flagName }}</optiarton>                                  
                            @endforeach
                        </select>
                        @error('categoryId')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror

                            </div>
                            <!-- /.form-group -->
                            
                            <div class="form-group">
                              <label for="salesPrice">Sales Price</label>
                              <input type="number" name="salesPrice"  id="salesPrice" class="form-control @error('salesPrice') is-invalid @enderror" value="{{ $model->salesPrice }}">
                              @error('salesPrice')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror

                            </div>
                            <!-- /.form-group -->
                            
                            <div class="form-group">
                              <label for="parentProductId">Parent Product</label><br>
                            <select type="text" name="parentProductId"  id="parentProductId" class="form-control select2bs4 @error('parentProductId') is-invalid @enderror" 
                                    placeholder="Select Parent Product (Optional)" autocomplete="off">
                                    <option value='' >Select Parent Product (Optional)</option>
                                    @foreach (Products::where('productType',$model->productType)->where("id",'<>',$model->id)->get() as $products)
                                      <option {{ ($model->parentProductId == $products->id) ? "selected" : "" }} value="{{ $products->id }}">{{ $products->name }}</option>                                  
                                @endforeach
                            </select>
                            @error('parentProductId')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                            <!-- /.form-group -->
                            
                          </div>
                          <div class="col-md-6">
                            
                            <div class="form-group">
                              
                              <label for="name">Name</label>
                              <input type="text" name="name"  id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $model->name }}">
                              @error('id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                          <!-- /.form-group -->
                            <div class="form-group">
                              <label for="unitId">Unit</label>
                            <br>
                            <select type="text" name="unitId"  id="unitId" class="form-control select2bs4 @error('unitId') is-invalid @enderror" 
                                placeholder="Select Unit" autocomplete="off">
                                <option value="" >Select Unit</option>
                                @foreach (SystemFlags::where('flagType','UNIT')->get() as $Unit)
                                  <option {{ ($model->unitId == $Unit->id) ? "selected" : "" }} value="{{ $Unit->id }}">{{ $Unit->flagName }}</option>                                  
                            @endforeach
                        </select>
                        @error('unitId')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror

                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                              <label for="purchasePrice">Purchase Price</label>
                              <input type="number" name="purchasePrice"  id="purchasePrice" class="form-control @error('purchasePrice') is-invalid @enderror" value="{{ $model->purchasePrice }}">
                              @error('purchasePrice')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                            </div>
                            <!-- /.form-group -->

                          </div>
                        </div>
                  </div>
                  <div class="card-footer">
                  @if(userHasPermission('Add Product'))
                      <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{$model->id == '' ? ' Create' : " Update"}}</button>
                  @endif
                  </div>
                </form>  
              </div>  
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="productsDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>ParentName</th>
                              <th>Type</th>
                              <th>Category</th>
                              <th>Unit</th>
                              <th>Saleing Price</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse (Products::all() as $product)
                              <tr>
                                  <td>{{ $product->id }}</td>
                                  <td>{{ $product->name }}</td>
                                  <td>{{ $product->parentProductName->name??'' }}</td>
                                  <td>{{ $product->productType }}</td>
                                  <td>{{ $product->CategoryName->flagName??'' }}</td><!--<td>{{ SystemFlags::getSystemFlagName($product->categoryId) }}</td>-->
                                  <td>{{ $product->UnitName->flagName??'' }}</td><!--<td>{{ SystemFlags::getSystemFlagName($product->unitId) }}</td>-->
                                  <td>{{ $product->salesPrice }}</td>
                                  <td>
                                     @if(userHasPermission('Edit Product'))
                                      <a href="{{ route('products.edit', $product->id) }}" class="btn btn-sm btn-warning">Edit</a>
                                     @endif
                                     @if(userHasPermission('Delete Product'))
                                      <a href="{{ route('products.destroy', $product->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                     @endif
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $('#parentProductId').select2();
  $('#categoryId').select2();
  $('#unitId').select2();
  $(function () {
    $('#productsDataTable').DataTable({
      "initComplete": function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
      "dom": 'Bfrtip',
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

  // $("#salesPrice").change(function(){
  //   debugger;
  //   var salesPrice = parseInt($("#salesPrice").val());
  //   var purchasePrice = parseInt($("#purchasePrice").val());
  //   if ( !isNaN(purchasePrice) && purchasePrice != 0 && salesPrice < purchasePrice  ){
  //     $("salesPrice").css("border", "red");
  //   }else{
  //     $("salesPrice").css("background-color", "white");
  //   }
  // });
</script>
@endsection()