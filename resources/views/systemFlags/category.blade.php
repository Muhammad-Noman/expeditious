<?php

use App\Models\SystemFlags;


  $systemFlag = $_GET['systemFlag'];
  $parentCategory = SystemFlags::where('flagType','CATEGORY')->get();
?>
@extends('master')

@section('title')
Category
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Categorys</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card">  
            @if(isset($categoryData) )
            <form method="POST" action="{{ route('systemFlags.storeCategory') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="name">Category Id</label>
                        <input type="text" name="id"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ $categoryData->id }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    
                    <div class="form-group">
                        <input type="hidden" name="flagType" style="width:25%"  id="flagType" 
                        class="form-control @error('flagType') is-invalid @enderror" value="<?php echo $systemFlag;?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="name">Category Name</label>
                        <input type="text" name="flagName"  style="width:25%"  id="flagName" class="form-control @error('flagName') is-invalid @enderror" 
                                value="{{ $categoryData->flagName }}" required placeholder="Category Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="parentId">Parent Category</label>
                        <select type="text" name="parentId"  style="width:25%"  id="parentId" class="form-control @error('parentId') is-invalid @enderror" 
                                placeholder="Parent Category" autocomplete="off">
                                <option value='0'>Select Category</option>
                            @foreach ($parentCategory as $Category)
                                @if($Category->id == $categoryData->parentId)
                                <option selected value='{{$Category->id}}'>{{ $Category->flagName }}</option>                                    
                                @else
                                    <option value='{{$Category->id}}'>{{ $Category->flagName }}</option>
                                @endif                                
                            @endforeach
                        </select>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Update Category</button>
                </div>
              </form>
            @else
            <form method="POST" action="{{ route('systemFlags.storeCategory') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="id">Category Id</label>
                        <input type="text" name="id" style="width:25%"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ old('id') }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="flagType" style="width:25%"  id="flagType" 
                        class="form-control @error('flagType') is-invalid @enderror" value="<?php echo $systemFlag;?>" readonly>
                    </div>
                      
                    <div class="form-group">
                        <label for="name">Category Name</label>
                        <input type="text" name="flagName"  style="width:25%"  id="flagName" class="form-control @error('name') is-invalid @enderror" 
                              value="{{ old('name') }}" required placeholder="Category Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Parent Category</label>
                        <select type="text" name="parentId"  style="width:25%"  id="parentId" class="form-control @error('parentId') is-invalid @enderror" 
                                placeholder="Parent Category" autocomplete="off">
                                <option value='0'>Select Category</option>
                            @foreach ($parentCategory as $Category)
                                <option value='{{$Category->id}}'>{{ $Category->flagName }}</option>                                    
                            @endforeach
                        </select>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                @if(userHasPermission('Add Product Category'))
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Create Category</button>
                @endif
                  </div>
              </form>
            @endif
              
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="CategorysDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Category</th>
                              <th>Date Posted</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($categorys as $Category)
                              <tr>
                                  <td>{{ $Category->id }}</td>
                                  <td>{{ $Category->flagName }}</td>
                                  <td>{{ $Category->created_at }}</td>
                                  <td>
                                  @if(userHasPermission('Edit Product Category'))
                                      <a href="{{ route('systemFlags.edit', $Category->id) }}" class="btn btn-sm btn-warning">Edit Category</a>
                                  @endif
                                  @if(userHasPermission('Delete Product Category'))
                                      <a href="{{ route('systemFlags.destroy', $Category->id) }}" class="btn btn-sm btn-danger">Delete Category</a>
                                  @endif 
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $(function () {
    $('#CategorysDataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });

  $( document ).ready(function() {
    $('#parentId').select2();
    // $('#parentId').select2({
    //     placeholder: 'Select Parenter',
    //     ajax: {
    //         url: '/ajax-autocomplete-search',
    //         data: function (params) {
    //         var query = {
    //             search: params.term,
    //             type: 'public',
    //             table: 'system_flags',
    //             column1:"id",
    //             column2:"flagName",
    //             where: "flagType='CATEGORY'"
    //         }
    //         return query;},
    //         dataType: 'json',
    //         delay: 250,
    //         processResults: function (data) {
    //             return {
    //                 results: $.map(data, function (item) {
    //                     return {
    //                         text: item.flagName,
    //                         id: item.id
    //                     }
    //                 })
    //             };
    //         },
    //         cache: true
    //     }
    // });
});
  

</script>

@endsection()