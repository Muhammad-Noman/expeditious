@extends('master')

@section('title')
Unit
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Units</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol style="display:none;" class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v3</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-12">
            <div class="card">  
            @if(isset($UnitData) )
            <form method="POST" action="{{ route('systemFlags.storeUnit') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="name">Unit Id</label>
                        <input type="text" name="id"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ $UnitData->id }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    
                    <div class="form-group">
                        <input type="hidden" name="flagType" style="width:25%"  id="flagType" class="form-control @error('flagType') is-invalid @enderror" value="UNIT" readonly>
                    </div>
                    <div class="form-group">
                        <label for="name">Unit Name</label>
                        <input type="text" name="flagName"  style="width:25%"  id="flagName" class="form-control @error('flagName') is-invalid @enderror" 
                                value="{{ $UnitData->flagName }}" required placeholder="Unit Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Update Units</button>
                </div>
              </form>
            @else
            <form method="POST" action="{{ route('systemFlags.storeUnit') }}">
                @csrf
                <div class="card-body">
                      <div class="form-group">
                        <label for="id">Unit Id</label>
                        <input type="text" name="id" style="width:25%"  id="id" class="form-control @error('id') is-invalid @enderror" value="{{ old('id') }}" readonly>
                        @error('id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="flagType" style="width:25%"  id="flagType" class="form-control @error('flagType') is-invalid @enderror" value="UNIT" readonly>
                    </div>
                      
                    <div class="form-group">
                        <label for="name">Unit Name</label>
                        <input type="text" name="flagName"  style="width:25%"  id="flagName" class="form-control @error('name') is-invalid @enderror" 
                              value="{{ old('name') }}" required placeholder="Unit Name" autocomplete="off">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="card-footer">
                @if(userHasPermission('Add Product Units'))
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Create Units</button>
                @endif
                  </div>
              </form>
            @endif
              
            </div>
            <div class="card">                              
              <div class="card-body table-responsive p-0">
                  <table id="unitsDataTable" class="table table-hover table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Unit</th>
                              <th>Date Posted</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          @forelse ($units as $unit)
                              <tr>
                                  <td>{{ $unit->id }}</td>
                                  <td>{{ $unit->flagName }}</td>
                                  <td>{{ $unit->created_at }}</td>
                                  <td>
                                  @if(userHasPermission('Edit Product Units'))
                                      <a href="{{ route('systemFlags.edit', $unit->id) }}" class="btn btn-sm btn-warning">Edit unit</a>
                                  @endif   
                                  @if(userHasPermission('Delete Product Units'))
                                      <a href="{{ route('systemFlags.destroy', $unit->id) }}" class="btn btn-sm btn-danger">Delete unit</a>
                                  @endif 
                                    </td>
                              </tr>
                          @empty
                              <tr>No Result Found</tr>
                          @endforelse
                      </tbody>
                  </table>
              </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  @endsection()

@section('scripts')
<script>
  $(function () {
    $('#unitsDataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection()